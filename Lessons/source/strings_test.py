#!python

from strings import contains, find_index, find_all_indexes
import pytest
import sys

# class StringsTest(unittest.TestCase):
class TestStrings:
    # TODO: Write more positive test cases with assert is True statements
    @pytest.mark.parametrize('text,pattern', [
        ('abc', ''), # all strings contain empty string
        ('abc', 'a'), # single letters are easy
        ('abc', 'b'),
        ('abc', 'c'),
        ('abc', 'ab'), # multiple letters are harder
        ('abc', 'bc'),
        ('abc', 'abc'), # all strings contain themselves
        ('aaa', 'a'), # multiple occurrences
        ('aaa', 'aa'), # overlapping pattern
    ])
    def test_contains_with_matching_patterns(self, text, pattern):
        assert contains(text, pattern) is True

    # TODO: Write more negative test cases with assert is False statements
    @pytest.mark.parametrize('text,pattern', [
        ('abc', 'z'), # remember to test other letters
        ('abc', 'ac'), # important to test close cases
        ('abc', 'az'), # first letter, but not last
        ('abc', 'abz'),  # first 2 letters, but not last
    ])
    def test_contains_with_non_matching_patterns(self, text, pattern):
        # Negative test cases (counterexamples) with non-matching patterns
        assert contains(text, pattern) is False

    # TODO: Write more test cases that check complex patterns or edge cases
    # You'll need a lot more than this to test your algorithm's robustness
    @pytest.mark.parametrize('text,pattern', [
        ('ababc', 'ab'), # multiple occurrences
        ('banana', 'na'), # multiple occurrences
        ('ababc', 'abc'), # overlapping prefix
        ('bananas', 'nas') # overlapping prefix
    ])
    def test_contains_with_complex_patterns(self, text, pattern):
        # Difficult test cases (examples) with complex patterns
        assert contains(text, pattern) is True

    # TODO: Write more positive test cases with assert equal int statements
    @pytest.mark.parametrize('text,pattern,expected', [
        ('abc', '', 0), # all strings contain empty string
        ('abc', 'a', 0), # single letters are easy
        ('abc', 'b', 1),
        ('abc', 'c', 2),
        ('abc', 'ab', 0), # multiple letters are harder
        ('abc', 'bc', 1),
        ('abc', 'abc', 0), # all strings contain themselves
        ('aaa', 'a', 0), # multiple occurrences
        ('aaa', 'aa', 0), # overlapping pattern
    ])
    def test_find_index_with_matching_patterns(self, text, pattern, expected):
        # Positive test cases (examples) with matching patterns
        assert find_index(text, pattern) == expected

    # TODO: Write more negative test cases with assert is None statements
    @pytest.mark.parametrize('text,pattern,expected', [
        ('abc', 'z', None), # remember to test other letters
        ('abc', 'ac', None), # important to test close cases
        ('abc', 'az', None), # first letter, but not last
        ('abc', 'abz', None) # first 2 letters, but not last
    ])
    def test_find_index_with_non_matching_patterns(self, text, pattern, expected):
        # Negative test cases (counterexamples) with non-matching patterns
        assert find_index(text, pattern) == expected

    # TODO: Write more test cases that check complex patterns or edge cases
    # You'll need a lot more than this to test your algorithm's robustness
    @pytest.mark.parametrize('text,pattern,expected', [
        ('ababc', 'abc', 2), # overlapping prefix
        ('bananas', 'nas', 4), # overlapping prefix
        ('abcabcabc', 'abc', 0), # multiple occurrences
        ('abcabcab', 'abc', 0), # multiple occurrences
        ('abcabcdef', 'abcd', 3), # overlapping prefix
        ('abcabcdef', 'abcdef', 3), # overlapping prefix
        ('abcabcdabcde', 'abcde', 7), # overlapping prefix
        ('abcabcdabcde', 'abcd', 3), # multiple occurrences, overlapping prefix
        ('abra cadabra', 'abra', 0), # multiple occurrences
        ('abra cadabra', 'adab', 6), # overlapping prefix
    ])
    def test_find_index_with_complex_patterns(self, text, pattern, expected):
        # Difficult test cases (examples) with complex patterns
        assert find_index(text, pattern) == expected

    # TODO: Write more positive test cases with assert equal list statements
    @pytest.mark.parametrize('text,pattern,expected', [
        ('abc', '', [0, 1, 2]), # all strings contain empty string
        ('abc', 'a', [0]), # single letters are easy
        ('abc', 'b', [1]),
        ('abc', 'c', [2]),
        ('abc', 'ab', [0]), # multiple letters are harder
        ('abc', 'bc', [1]),
        ('abc', 'abc', [0]), # all strings contain themselves
        ('aaa', 'a', [0, 1, 2]), # multiple occurrences
        ('aaa', 'aa', [0, 1]), # overlapping pattern
    ])
    def test_find_all_indexes_with_matching_patterns(self, text, pattern, expected):
        # Positive test cases (examples) with matching patterns
        assert find_all_indexes(text, pattern) == expected

    # TODO: Write more negative test cases with assert equal list statements
    @pytest.mark.parametrize('text,pattern,expected', [
        ('abc', 'z', []), # remember to test other letters
        ('abc', 'ac', []), # important to test close cases
        ('abc', 'az', []), # first letter, but not last
        ('abc', 'abz', []), # first 2 letters, but not last
    ])

    # TODO: Write more test cases that check complex patterns or edge cases
    # You'll need a lot more than this to test your algorithm's robustness
    def test_find_all_indexes_with_non_matching_patterns(self, text, pattern, expected):
        # Negative test cases (counterexamples) with non-matching patterns
        assert find_all_indexes(text, pattern) == expected
    @pytest.mark.parametrize('text,pattern,expected', [
        ('ababc', 'abc', [2]), # overlapping prefix
        ('bananas', 'nas', [4]), # overlapping prefix
        ('abcabcabc', 'abc', [0, 3, 6]), # multiple occurrences
        ('abcabcab', 'abc', [0, 3]), # multiple occurrences
        ('abcabcdef', 'abcd', [3]), # overlapping prefix
        ('abcabcdef', 'abcdef', [3]), # overlapping prefix
        ('abcabcdabcde', 'abcde', [7]), # overlapping prefix
        ('abcabcdabcde', 'abcd', [3, 7]), # multiple occurrences, overlapping prefix
        ('abra cadabra', 'abra', [0, 8]), # multiple occurrences
        ('abra cadabra', 'adab', [6]), # overlapping prefix
    ])
    def test_find_all_indexes_with_complex_patterns(self, text, pattern, expected):
        # Difficult test cases (examples) with complex patterns
        assert find_all_indexes(text, pattern) == expected


if __name__ == '__main__':
    args = sys.argv[1:]
    args.insert(0, __file__)

    pytest.main(args)
