#!python

def contains(text, pattern):
    """Determines if a pattern occurs in a given text

    Args:
        text (str): The string to search
        pattern (str): The string that is being checked if exists in `text`

    Returns:
        bool: True if exists, False if not
    """
    assert isinstance(text, str), 'text is not a string: {}'.format(text)
    assert isinstance(pattern, str), 'pattern is not a string: {}'.format(text)

    # None is not found in find_index
    return find_index(text, pattern) is not None

def find_index(text, pattern):
    """Finds the starting index of the first occurence of a pattern in a given text

    Args:
        text (str): The string to search
        pattern ([type]): The string that is being checked if exists in `text`

    Returns:
        int: The index of the start
        None: If not found
    """
    assert isinstance(text, str), 'text is not a string: {}'.format(text)
    assert isinstance(pattern, str), 'pattern is not a string: {}'.format(text)

    # Get the first matching index, but default to None if not
    return next(_find_indexes(text, pattern), None)


def find_all_indexes(text, pattern):
    """Finds the starting indices of all occurrences of a pattern in a given text

    Args:
        text (str): The string to search
        pattern (str): The string that is being checked if exists in `text`

    Returns:
        list: The starting indices of each occurence
    """
    assert isinstance(text, str), 'text is not a string: {}'.format(text)
    assert isinstance(pattern, str), 'pattern is not a string: {}'.format(text)

    # Put all matching indices in a list
    return list(_find_indexes(text, pattern))

def _find_indexes(text, pattern):
    """Yields for every matching index"""
    # Return every index if pattern is empty because "all strings contain empty string"
    if pattern == '':
        yield from range(len(text))
        return

    # Find matching indices
    for index in range(len(text) - len(pattern) + 1):
        # Loop over every character in potential match
        for char, pat_char in zip((text[i] for i in range(index, index + len(pattern))), pattern):
            if char is not pat_char:
                break
        else:
            yield index


def test_string_algorithms(text, pattern):
    found = contains(text, pattern)
    print('contains({!r}, {!r}) => {}'.format(text, pattern, found))

    index = find_index(text, pattern)
    print('find_index({!r}, {!r}) => {}'.format(text, pattern, index))

    indexes = find_all_indexes(text, pattern)
    print('find_all_indexes({!r}, {!r}) => {}'.format(text, pattern, indexes))


def main():
    """Read command-line arguments and test string searching algorithms."""
    import sys
    args = sys.argv[1:]  # Ignore script file name
    if len(args) == 2:
        text = args[0]
        pattern = args[1]
        test_string_algorithms(text, pattern)
    else:
        script = sys.argv[0]
        print('Usage: {} text pattern'.format(script))
        print('Searches for occurrences of pattern in text')
        print("\nExample: {} 'abra cadabra' 'abra'".format(script))
        print("contains('abra cadabra', 'abra') => True")
        print("find_index('abra cadabra', 'abra') => 0")
        print("find_all_indexes('abra cadabra', 'abra') => [0, 8]")


if __name__ == '__main__':
    main()
