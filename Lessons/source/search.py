#!python

def linear_search(array, item):
    """Searches for an item in an array linearly

    Running time: O(n)

    Args:
        array (list): The array to search
        item (any): The elemnt being searched for. Must be a tye that is comparable with `==`

    Returns:
        int: THe index of `item` if it is found
        None: If `item is not found
    """
    # return linear_search_iterative(array, item)
    return linear_search_recursive(array, item)

def linear_search_iterative(array, item):
    """Searches for an item in an array using an iterative linear approach"""
    # loop over all array values until item is found
    for index, value in enumerate(array):
        if item == value:
            return index  # found
    return None  # not found

def linear_search_recursive(array, item, index=0):
    """Searches for an item in an array using a recursive linear approach"""
    if index >= len(array):
        return None
    elif array[index] == item:
        return index
    else:
        return linear_search_recursive(array, item, index + 1)

def binary_search(array, item):
    """Searches for an item in an array using binary search

    Running time O(log n)

    Args:
        array (list): The array to search
        item (any): The element being searched for. Must be a type that is comparable with `==`

    Returns:
        int: The index of `item` if it is found
        None: If `item` is not found
    """
    # return binary_search_iterative(array, item)
    return binary_search_recursive(array, item)


def binary_search_iterative(array, item):
    """Searches for an item in an array using an iterative binary search approach"""
    left = 0
    right = len(array) - 1

    while left <= right:
        middle = (right + left) // 2

        if array[middle] == item:
            return middle
        elif array[middle] < item:
            left = middle + 1
        else:
            right = middle - 1

    return None

def binary_search_recursive(array, item, left=None, right=None):
    """Searches for an item in an array using a recursive binary search approach"""
    if left == None:
        left = 0
    if right == None:
        right = len(array) - 1

    middle = (right + left) // 2

    if left > right:
        return None
    if array[middle] == item:
        return middle
    elif array[middle] < item:
        return binary_search_recursive(array, item, middle + 1, right)
    else:
        return binary_search_recursive(array, item, left, middle - 1)

