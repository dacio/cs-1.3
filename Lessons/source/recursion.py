#!python

def factorial(n):
    """Calculates the factorial of a given number

    Args:
        n (int): The number to calculate the factorial of

    Raises:
        ValueError: If `n` is not an integer or is less than 0

    Returns:
        int: The calculated factorial
    """
    # check if n is negative or not an integer (invalid input)
    if not isinstance(n, int) or n < 0:
        raise ValueError('factorial is undefined for n = {}'.format(n))
    # implement factorial_iterative and factorial_recursive below, then
    # change this to call your implementation to verify it passes all tests
    return factorial_iterative(n)
    # return factorial_recursive(n)


def factorial_iterative(n):
    """Caclulate the factorial of a given number iteratively"""
    product = 1

    # Loop over important numbers
    for num in range(2, n + 1):
        product *= num

    return product


def factorial_recursive(n):
    """Returns the factorial of a given number calculated recursively"""
    # check if n is one of the base cases
    if n == 0 or n == 1:
        return 1
    # check if n is an integer larger than the base cases
    elif n > 1:
        # call function recursively
        return n * factorial_recursive(n - 1)


def main():
    """Provides a simple CLI for `factorial`"""
    import sys
    args = sys.argv[1:]  # Ignore script file name
    if len(args) == 1:
        num = int(args[0])
        result = factorial(num)
        print('factorial({}) => {}'.format(num, result))
    else:
        print('Usage: {} number'.format(sys.argv[0]))


if __name__ == '__main__':
    main()
