#!python

import math
from linkedlist import LinkedList

class HashTable(object):

    def __init__(self, init_size=8, items=None):
        """Initialize this hash table with the given initial size."""
        self.buckets = [LinkedList() for i in range(init_size)]
        self.size = 0  # Number of key-value entries

        try:
            for key, value in items:
                self.set(key, value)
        except TypeError:
            pass

    def __str__(self):
        """Creates a formatted string representation of this hash table."""
        items = ['{!r}: {!r}'.format(key, val) for key, val in self.items()]
        return '{' + ', '.join(items) + '}'

    def __repr__(self):
        """Creates a string representation of this hash table."""
        return 'HashTable({!r})'.format(list(self.items()))

    def _bucket_index(self, key):
        """Calculates the index of the bucket where the given key would be stored."""
        return hash(key) % len(self.buckets)

    def load_factor(self):
        """Return the load factor, the ratio of number of entries to buckets.
        Best and worst case running time: O(1) alawys; .size and len() are constant"""
        return self.size / len(self.buckets)

    def keys(self):
        """Create a generator of all the keys the hash table

        Best and worst case running time: O(n) always; loop over each (k, v)
        """
        items = self.items()

        return (key for key, _ in items)

    def values(self):
        """Create a generator of all the keys the hash table

        Best and worst case running time: O(n) always; loop over each (k, v)
        """
        items = self.items()

        return (value for _, value in items)

    def items(self):
        """Create a generator of all the keys the hash table

        Best and worst case running time: O(n) always; loop over each (k, v)
        """
        buckets = self.buckets

        return (item for bucket in buckets for item in bucket.items())

    def length(self):
        """Get the number of key-value entries.

        Best and worst case running time: O(1) always; size is tracked.
        """
        return self.size

    def contains(self, key):
        """Calculates a boolean if this hash table contains the given key or not.

        Best case running time: O(1) the key is the first element in bucket
        Worst case running time: O(k) the key is the last element in bucket
        """
        # Find the bucket the given key belongs in
        index = self._bucket_index(key)
        bucket = self.buckets[index]
        # Check if an entry with the given key exists in that bucket
        entry = bucket.find(lambda key_value: key_value[0] == key)
        return entry is not None  # True or False

    def get(self, key):
        """Return the value associated with the given key, or raise KeyError.

        Best case running time: O(1) the key is the first element in bucket
        Worst case running time: O(k) the key is the last element in bucket
        """
        # Find the bucket the given key belongs in
        index = self._bucket_index(key)
        bucket = self.buckets[index]
        # Find the entry with the given key in that bucket, if one exists
        entry = bucket.find(lambda key_value: key_value[0] == key)
        if entry is not None:  # Found
            # Return the given key's associated value
            assert isinstance(entry, tuple)
            assert len(entry) == 2
            return entry[1]
        else:  # Not found
            raise KeyError('Key not found: {}'.format(key))

    def set(self, key, value):
        """Inserts or updates the given key with its associated value.
        Best case running time: O(1) the key is the first element in bucket
        Worst case running time: O(k) the key is the last element in bucket
        """
        # Find the bucket the given key belongs in
        index = self._bucket_index(key)
        bucket = self.buckets[index]
        # Find the entry with the given key in that bucket, if one exists
        # Check if an entry with the given key exists in that bucket
        entry = bucket.find(lambda key_value: key_value[0] == key)
        if entry is not None:  # Found
            # In this case, the given key's value is being updated
            # Remove the old key-value entry from the bucket first
            bucket.delete(entry)
        else:
            self.size += 1
        # Insert the new key-value entry into the bucket in either case
        bucket.append((key, value))

        if (self.load_factor() > 0.75):
            self._resize()

    def delete(self, key):
        """Delete the given key and its associated value, or raise KeyError.

        Best case running time: O(1) the key is the first element in bucket
        Worst case running time: O(k) the key is the last element in bucket
        """
        # Find the bucket the given key belongs in
        index = self._bucket_index(key)
        bucket = self.buckets[index]
        # Find the entry with the given key in that bucket, if one exists
        entry = bucket.find(lambda key_value: key_value[0] == key)
        if entry is not None:  # Found
            # Remove the key-value entry from the bucket
            bucket.delete(entry)
            self.size -= 1
        else:  # Not found
            raise KeyError('Key not found: {}'.format(key))

    def _resize(self, new_size=None):
        """Resize this hash table's buckets and rehash all key-value entries.

        Should be called automatically when load factor exceeds a threshold
        such as 0.75 after an insertion (when set is called with a new key).

        Best and worst case running time: O(n) always; loops over every element
        Best and worst case space usage: O(n) always; keeps old buckets to loop over
        """
        # If unspecified, choose new size dynamically based on current size
        if new_size is None:
            new_size = len(self.buckets) * 2  # Double size
        # Option to reduce size if buckets are sparsely filled (low load factor)
        elif new_size is 0:
            new_size = math.ceil(len(self.buckets) / 2)  # Half size, round up to be safe

        self.__init__(new_size, self.items())

def test_hash_table():
    ht = HashTable(4)
    print('HashTable: ' + str(ht))

    print('Setting entries:')
    ht.set('I', 1)
    print('set(I, 1): ' + str(ht))
    ht.set('V', 5)
    print('set(V, 5): ' + str(ht))
    print('size: ' + str(ht.size))
    print('length: ' + str(ht.length()))
    print('buckets: ' + str(len(ht.buckets)))
    print('load_factor: ' + str(ht.load_factor()))
    ht.set('X', 10)
    print('set(X, 10): ' + str(ht))
    ht.set('L', 50)  # Should trigger resize
    print('set(L, 50): ' + str(ht))
    print('size: ' + str(ht.size))
    print('length: ' + str(ht.length()))
    print('buckets: ' + str(len(ht.buckets)))
    print('load_factor: ' + str(ht.load_factor()))

    print('Getting entries:')
    print('get(I): ' + str(ht.get('I')))
    print('get(V): ' + str(ht.get('V')))
    print('get(X): ' + str(ht.get('X')))
    print('get(L): ' + str(ht.get('L')))
    print('contains(X): ' + str(ht.contains('X')))
    print('contains(Z): ' + str(ht.contains('Z')))

    print('Deleting entries:')
    ht.delete('I')
    print('delete(I): ' + str(ht))
    ht.delete('V')
    print('delete(V): ' + str(ht))
    ht.delete('X')
    print('delete(X): ' + str(ht))
    ht.delete('L')
    print('delete(L): ' + str(ht))
    print('contains(X): ' + str(ht.contains('X')))
    print('size: ' + str(ht.size))
    print('length: ' + str(ht.length()))
    print('buckets: ' + str(len(ht.buckets)))
    print('load_factor: ' + str(ht.load_factor()))


if __name__ == '__main__':
    test_hash_table()
