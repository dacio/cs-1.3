#!python

from my_set import Set
import unittest

class SetTest(unittest.TestCase):
    def test_init(self):
        Set()
        Set(['a', 'b'])

    def test_len(self):
        # Should initialize with no elements
        s = Set()
        assert len(s) == 0

        # Adding new element should increement length
        s.add('a')
        assert len(s) == 1

        # Adding duplicate element shouldn't modify length
        s.add('a')
        assert len(s) == 1

        # Initializing with unique elements should set length to number of elements
        s = Set(['a', 'b', 'c'])
        assert len(s) == 3

        # Initializing with non-unique elements should set length to number of unique elements
        s = Set(['a', 'b', 'a'])
        assert len(s) == 2

    def test_keys(self):
        s = Set()
        self.assertCountEqual(list(s), [])

        s.add('a')
        self.assertCountEqual(list(s), ['a'])

        s.add('a')
        self.assertCountEqual(list(s), ['a'])

        s = Set(['a', 'b', 'c'])
        self.assertCountEqual(list(s), ['a', 'b', 'c'])

        s = Set(['a', 'b', 'a'])
        self.assertCountEqual(list(s), ['a', 'b'])


    def test_remove(self):
        # Should remove key from set
        s = Set(['a', 'b', 'c'])
        s.remove('a')

        self.assertCountEqual(list(s), ['b', 'c'])

        # Should raise error when removing non-exisistent key
        with self.assertRaises(KeyError):
            s.remove('a')

    def test_union(self):
        s1 = Set(['a', 'b'])
        s2 = Set(['b', 'c'])

        s = s1 | s2

        self.assertCountEqual(list(s), ['a', 'b', 'c'])

    def test_intersection(self):
        s1 = Set(['a', 'b'])
        s2 = Set(['b', 'c'])

        s = s1 & s2

        self.assertCountEqual(list(s), ['b'])

    def test_difference(self):
        s1 = Set(['a', 'b'])
        s2 = Set(['b', 'c'])

        s = s1 - s2

        self.assertCountEqual(list(s), ['a'])

    def test_issubset(self):
        s1 = Set(['a', 'b'])
        s2 = Set(['a', 'b', 'c'])

        assert (s1 <= s2) is True

        s1 = Set(['a', 'b', 'c'])
        s2 = Set(['a', 'b'])

        assert (s1 <= s2) is False

        s1 = Set(['a', 'b'])
        s2 = Set(['a', 'b'])

        assert (s1 <= s1) is True

    def test_issuperset(self):
        s1 = Set(['a', 'b'])
        s2 = Set(['a', 'b', 'c'])

        assert (s1 >= s2) is False

        s1 = Set(['a', 'b', 'c'])
        s2 = Set(['a', 'b'])

        assert (s1 >= s2) is True

        s1 = Set(['a',   'b'])
        s2 = Set(['a', 'b'])

        assert (s1 >= s1) is True

    def test_equal(self):
        s1 = Set(['a', 'b'])
        s2 = Set(['a', 'b', 'c'])

        assert (s1 == s2) is False
        assert (s2 == s1) is False

        s1 = Set(['a', 'b', 'c'])
        s2 = Set(['a', 'b'])

        assert (s1 == s2) is False
        assert (s2 == s1) is False

        s1 = Set(['a', 'b'])

        assert (s1 == s1) is True

        s1 = Set(['a', 'b'])
        s2 = Set(['a', 'b'])

        assert (s1 == s2) is True
        assert (s2 == s1) is True

    def test_symmetric_difference(self):
        s1 = Set(['a', 'b'])
        s2 = Set(['b', 'c'])

        s = s1 ^ s2

        self.assertCountEqual(list(s), ['a', 'c'])
