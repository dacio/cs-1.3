#!python

from hashtable import HashTable
from typing import Hashable, Optional, Iterable
from itertools import chain

class Set:
    def __init__(self: 'Set', keys: Optional[Iterable[Hashable]] = None) -> None:
        try:
            items = map(lambda key: (key, None), keys)
        except TypeError:
            items = None

        self.hashtable = HashTable(items=items)

    def __repr__(self: 'Set') -> str:
        return '{' + ', '.join(map(repr, self)) + '}'

    def __str__(self: 'Set') -> str:
        if len(self) == 0:
            return 'Set()'

        return f'Set({ repr(list(self)) })'

    def add(self: 'Set', key: Hashable):
        self.hashtable.set(key, None)

    def remove(self: 'Set', key: Hashable) -> None:
        self.hashtable.delete(key)

    def contains(self: 'Set', key: Hashable) -> bool:
        return self.hashtable.contains(key)

    def length(self: 'Set') -> int:
        return self.hashtable.length()

    def union(self: 'Set', *others: 'Set') -> 'Set':
        keys = chain(self, *others)
        return Set(keys)

    def intersection(self: 'Set', other: 'Set') -> 'Set':
        # Iterate over smaller set
        if len(self) > len(other):
            keys = filter(lambda key: key in self, other)
        else:
            keys = filter(lambda key: key in other, self)

        return Set(keys)

    def difference(self: 'Set', other: 'Set') -> 'Set':
        keys = filter(lambda key: key not in other, self)
        return Set(keys)

    def issubset(self: 'Set', other: 'Set') -> bool:
        return len(self) <= len(other) and len(self - other) == 0

    def issuperset(self: 'Set', other: 'Set') -> bool:
        return other <= self

    def equal(self: 'Set', other: 'Set') -> bool:
        return self is other or (self <= other and self >= other)


    def symmetric_difference(self: 'Set', other: 'Set') -> 'Set':
        keys = filter(lambda key: (key in self) is not (key in other), self | other)
        return Set(keys)

    __len__ = length
    __contains__ = contains
    __or__ = union
    __and__ = intersection
    __sub__ = difference
    __le__ = issubset
    __ge__ = issuperset
    __eq__ = equal
    __xor__ = symmetric_difference

    def __iter__(self: 'Set') -> Iterable:
        return self.hashtable.keys()

    def __lt__(self: 'Set', other: 'Set') -> bool:
        return self <= other and self != other

    def __gt__(self: 'Set', other: 'Set') -> bool:
        return self >= other and self != other
