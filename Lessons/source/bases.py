#!python

import string
# Hint: Use these string constants to encode/decode hexadecimal digits and more
# string.digits is '0123456789'
# string.hexdigits is '0123456789abcdefABCDEF'
# string.ascii_lowercase is 'abcdefghijklmnopqrstuvwxyz'
# string.ascii_uppercase is 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
# string.ascii_letters is ascii_lowercase + ascii_uppercase
# string.printable is digits + ascii_letters + punctuation + whitespace

NUM_2_SYMBOLS = string.digits + string.ascii_lowercase
SYMBOLS_2_NUM = { symbol: index for index, symbol in enumerate(NUM_2_SYMBOLS) }

def decode(digits, base):
    """Decode a number encoded in a given base to base 10

    Args:
        digits (str): The number to decode
        base (int): The base that `digits` is encoded in

    Returns:
        int: The decoded number
    """
    assert 2 <= base <= 36, 'base is out of range: {}'.format(base)

    # The value of the decoded number
    result = 0
    # The value of the current digit
    value = 1

    # Iterate reversed because value increases right to left
    for digit in reversed(digits):
        result += SYMBOLS_2_NUM[digit] * value
        value *= base

    return result


def encode(number, base):
    """Encode a number in base 10 to a given base

    Args:
        number (int): The number to encode
        base (int): The base to encode `number` with

    Returns:
        str: The encoded version of `number`
    """
    # Handle up to base 36 [0-9a-z]
    assert 2 <= base <= 36, 'base is out of range: {}'.format(base)
    # Handle unsigned numbers only for now
    assert number >= 0, 'number is negative: {}'.format(number)

    if number == 0:
        return '0'

    result = []

    # https://www.permadi.com/tutorial/numDecToHex/
    quotient = number

    while quotient != 0:
        """
        Divide last quotient by base
        Store quotient for iteration and use remainder for current digit
        """
        quotient, remainder = divmod(quotient, base)
        result.append(NUM_2_SYMBOLS[remainder])

    # Correct for reversed order and convert to string
    return ''.join(reversed(result))


def convert(digits, base1, base2):
    """Convert a number encoded in one base to another

    Args:
        digits (str): The encoded number to convert
        base1 (int): The base that `digits` is currently encoded in
        base2 (int): The base to encode `digits` to

    Returns:
        str: The newly encoded version of `digits`
    """
    assert 2 <= base1 <= 36, 'base1 is out of range: {}'.format(base1)
    assert 2 <= base2 <= 36, 'base2 is out of range: {}'.format(base2)

    return encode(decode(digits, base1), base2)


def main():
    """Provides a CLI for the above functions"""
    import argparse
    parser = argparse.ArgumentParser(
        description='Decode, encode and convert between base-encoded numbers'
    )

    subparsers = parser.add_subparsers(
        title='modes',
        required=True,
        dest='mode'
    )

    decode_parser = subparsers.add_parser(
        'decode',
        description='Decode any base-encoded number to a regular number'
    )
    decode_parser.add_argument(
        'digits',
        type=str,
        help='The base-encoded number to decode'
    )
    decode_parser.add_argument(
        'base',
        type=int,
        default=2,
        help='The base to decode \'digits\' from'
    )

    encode_parser = subparsers.add_parser(
        'encode',
        description='Encode a regular number to a base'
    )
    encode_parser.add_argument(
        'number',
        type=int,
        help='The number to encode'
    )
    encode_parser.add_argument(
        'base',
        type=int,
        default=2,
        help='The base to encode \'number\' by'
    )

    convert_parser = subparsers.add_parser(
        'convert',
        description='Convert any base-encoded number to another base'
    )
    convert_parser.add_argument(
        'digits',
        type=str,
        help='The base-encoded number to '
    )
    convert_parser.add_argument(
        'base1',
        type=str,
        help='The base \'digits\' is encoded in'
    )
    convert_parser.add_argument(
        'base2',
        type=str,
        help='The base to encode \'digits\' to'
    )

    args = parser.parse_args()

    mode = args.mode

    if mode == 'decode':
        print(decode(args.digits, args.base))
    elif mode == 'encode':
        print(encode(args.number, args.base))
    elif mode == 'convert':
        print(convert(args.digits, args.base1, args.base2))


if __name__ == '__main__':
    main()
