import unittest

class RedactWordsTest(unittest.TestCase):
    def test_simple(self):
        words = ['a', 'normal', 'fricking', 'sentence', 'dude']
        banned_words = ['fricking', 'dude']

        self.assertListEqual(list(redact_words(words, banned_words)), ['a', 'normal', 'sentence'])

        words = ['wow', 'holy', 'heck', 'this', 'is', 'annoying']
        banned_words = ['holy', 'heck']

        self.assertListEqual(list(redact_words(words, banned_words)), ['wow', 'this', 'is', 'annoying'])

    def test_no_banned(self):
        words = ['you', 'are', 'a', 'meanie']
        banned_words = []

        self.assertListEqual(list(redact_words(words, banned_words)), ['you', 'are', 'a', 'meanie'])

    def test_no_words(self):
        words = []
        banned_words = ['mate']

        self.assertListEqual(list(redact_words(words, banned_words)), [])

    def test_assert(self):
        words = True,
        banned_words = ['heck']

        with self.assertRaises(AssertionError):
            redact_words(words, banned_words)

        words = ['hello', 'world'],
        banned_words = None

        with self.assertRaises(AssertionError):
            redact_words(words, banned_words)

def redact_words(words: list, banned_words: list) -> list:
    """Removes words if other is banned

    Time complexity: O(m + n)

    Args:
        words (list): The input text.
        banned_words (list): The words to remove.

    Returns:
        list: The new sentence with banned words removed.
    """
    assert isinstance(words, list)
    assert isinstance(banned_words, list)
    banned_set = set(banned_words)
    # return (word for word in words if word not in banned_set)
    return filter(lambda word: word not in banned_set, words)
