from itertools import permutations

def norm(word):
  return ''.join(sorted(word.upper()))

word_dict = {}

for word in open('/usr/share/dict/words'):
  word_trim = word[:-1]
  word_norm = norm(word_trim)

  if word_norm in word_dict:
    word_dict[word_norm].append(word_trim)
  else:
    word_dict[word_norm] = [word_trim]


def unscramble(word):
  word_norm = norm(word)

  if word_norm in word_dict:
    return word_dict[word_norm][0]

if __name__ == "__main__":
  while True:
    print(unscramble(input('Enter a word: ')))
